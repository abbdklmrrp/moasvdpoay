package nc.nut.domain;

/**
 * @author Rysakova Anna
 */
public interface UserService {
    User findByName(String name);
}

package nc.nut.security;

/**
 * @author Rysakova Anna
 */
public interface AuthorityConstants {
    String ADMIN_VALUE = "admin";
    String USER_VALUE = "user";
    String MANAGER_VALUE = "manager";
    String SUPPORT_VALUE = "support";
    
}
